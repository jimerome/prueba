class Cuadrado:
        """ Un ejemplo de clase para los Cuadrados"""
        def __init__(self,l):
            self.lado = l
        
        def calculo_perimetro(self):
            return self.lado*4
        
        def calculo_area(self):
            return self.lado**2
        
        def __str__(self):
            return("El primetro del cuadrado es {perimetro} m²". format(perimetro=self.calculo_perimetro))

Cuadrado1=Cuadrado(5)
print(Cuadrado1)